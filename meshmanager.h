#ifndef MESHMANAGER_H
#define MESHMANAGER_H

#include <string>
#include <iostream>
#include <fstream>
#include <memory>
#include "mesh.h"

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

class MeshManager{
public:
    MeshManager();
    ~MeshManager();

    bool ImportMesh(std::string path, Mesh& obj); 
private:
    void RecursiveProcess(aiNode* node, const aiScene* scene);
    void ProcessMesh(aiMesh* mesh, const aiScene* scene);

    Mesh object;
    Mesh* lastUpdatedMesh;
    bool isRootMesh = true;
};

#endif
