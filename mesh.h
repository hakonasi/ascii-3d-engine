#ifndef MESH_H
#define MESH_H

#include <vector>
#include <memory>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

class Mesh
{
public:
    Mesh();
    Mesh(std::vector<glm::vec3> vertices,
        std::vector<int> indices,
        std::vector<glm::vec3> normals);
    Mesh(std::vector<glm::vec3> vertices,
		std::vector<glm::vec3> normals,
        std::vector<glm::vec3> colors,
        std::vector<int> indices);
    ~Mesh();

    std::vector<glm::vec3>& GetVertices();
    std::vector<glm::vec3>& GetColors();
    std::vector<glm::vec3>& GetNormals();
    std::vector<int>& GetIndices();
    std::vector<std::shared_ptr<Mesh>>& GetChildren();
    void AddChild(Mesh object);
    glm::mat4x4 GetTransform();
    void SetTransform(glm::mat4x4 transform);
private:
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> colors;
    std::vector<glm::vec3> normals;
    std::vector<int> indices;
    std::vector<std::shared_ptr<Mesh>> children;

    glm::mat4x4 transform;
};

#endif // MESH_H
