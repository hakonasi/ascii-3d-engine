#include "renderer.h"
#include "meshmanager.h"

#include <iostream>
#include <string>
#include <cmath>
#include <chrono>
#include <string>
#include <thread>

#include <unistd.h>     /* only for sleep() */
#include <ncurses.h>

#include <sys/ioctl.h>  // for getting a terminal size
#include <glm/gtx/string_cast.hpp>

using namespace std::chrono_literals;

typedef std::chrono::high_resolution_clock Time;
typedef std::chrono::milliseconds ms;
typedef std::chrono::duration<float> fsec;

WINDOW* win;
Renderer renderer;
MeshManager meshmanager;

Mesh object;

int scrwidth, scrheight;
float angle = 89.0f;

float yaw = 90.0f;
float pitch = 0.0f;

float fov = 60.0f;

glm::vec3 camDirection;
glm::vec3 lightDirection = glm::vec3(0.2f, 0.3f, 0.7f);

glm::vec3 camPos   = glm::vec3(0.0f, 0.0f, 3.5f);
glm::vec3 camFront = glm::vec3(0.0f, 0.0f, 1.0f);
glm::vec3 camUp    = glm::vec3(0.0f, 1.0f,  0.0f);

glm::mat4 transformMatrix;
glm::mat4 viewMatrix;
glm::mat4 projMatrix;

int kbhit(void)
{
    int ch = getch();

    if (ch != ERR) {
        ungetch(ch);
        return 1;
    } else {
        return 0;
    }
}

int main(int argc, char** argv)
{
    initscr();
    //start_color();
    noecho();
    cbreak();
    nodelay(stdscr, TRUE);
    scrollok(stdscr, TRUE);
    curs_set(0); // hide cursor

    init_pair(2, COLOR_MAGENTA, COLOR_WHITE);

    getmaxyx(stdscr, scrheight, scrwidth);
    win = newwin(scrheight, scrwidth, 0, 0);

    renderer.SetScreenSize(scrwidth, scrheight);
    projMatrix = glm::perspective(glm::radians(fov), ((float)scrwidth/(float)scrheight), 0.1f, 100.0f);
    std::string modelFilename;
    if(argc > 1){
        modelFilename = argv[1];
    } else {
        modelFilename = "cube.fbx";
    }
    meshmanager.ImportMesh(modelFilename, object);
    glm::vec3 pivot(0.0f, 0.0f, 0.0f);
    renderer.SetLightDirection(lightDirection);

    auto t0 = Time::now();

    bool isGame = true;
    // main loop
    while(isGame){
        if (kbhit()) {
            auto c = getch();
            switch(c){
            case 98:
                pivot.x += 0.2f;
                break;
            case 99:
                pivot.x -= 0.2f;
                break;
            case 'd':
                camPos -= glm::normalize(glm::cross(camFront, camUp)) * 0.5f;
                break;
            case 'a':
                camPos += glm::normalize(glm::cross(camFront, camUp)) * 0.5f;
                break;
            case 'w':
                camPos -= 0.5f * camFront;
                break;
            case 's':
                camPos += 0.5f * camFront;
                break;
            case 'r':
                pitch--;
                break;
            case 'f':
                pitch++;
                break;
            case 'y':
                lightDirection += glm::normalize(glm::vec3(0.1f, 0.0f, 0.0));
				renderer.SetLightDirection(lightDirection);
                break;
            case 'u':
                lightDirection += glm::normalize(glm::vec3(0.0f, 0.1f, 0.0f));
				renderer.SetLightDirection(lightDirection);
                 break;
            case 'i':
                lightDirection += glm::normalize(glm::vec3(0.0f, 0.0f, 0.1f));
				renderer.SetLightDirection(lightDirection);
                break;
            case 'h':
				fov++;
				projMatrix = glm::perspective(glm::radians(fov), ((float)scrwidth/(float)scrheight), 0.1f, 100.0f);
				break;
            case 'n':
				fov--;
				projMatrix = glm::perspective(glm::radians(fov), ((float)scrwidth/(float)scrheight), 0.1f, 100.0f);
				break;
            case 'q':
                isGame = false;
                break;
            case KEY_RESIZE:
                getmaxyx(stdscr, scrheight, scrwidth);
                wresize(win, scrheight, scrwidth);
                renderer.SetScreenSize(scrwidth, scrheight);
                projMatrix = glm::perspective(glm::radians(fov), (float)scrwidth/(float)scrheight, 0.1f, 100.0f);
                break;
            }
        } // if(kbhit)

        auto t1 = Time::now();
        fsec fs = t1 - t0;
        if(std::chrono::duration_cast<ms>(fs).count() >= 1000.0f/60.0f){
						
			camDirection.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
			camDirection.y = sin(glm::radians(pitch));
			camDirection.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
			camFront = glm::normalize(camDirection);
                
            t0 = Time::now();
            angle+=1.0f;
            if(angle >= 365.0f) angle = 0.0f;
            werase(win);
            viewMatrix = glm::lookAt(camPos, camPos - camFront, camUp);
            transformMatrix = glm::rotate(glm::mat4(1.0f), glm::radians(angle), glm::vec3(0.0, 1.0, 0.0))
                * glm::translate(glm::mat4(1.0f), pivot);

            // clearing the map
            renderer.ClearMap();
            renderer.Render(object, camFront, transformMatrix, viewMatrix, projMatrix);
            renderer.Print(win);

            mvprintw(0, 0, glm::to_string(camPos).c_str());
            mvprintw(1, 0, glm::to_string(lightDirection).c_str());
            mvprintw(2, 0, "%f", angle);

            wrefresh(win);
        }
    }
    endwin();
    return 0;
}
